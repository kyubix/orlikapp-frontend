const routes = [
  {
    path: '/',
    component: () => import('layouts/Layout.vue'),
    children: [
      {
        path: '/',
        name: 'index',
        component: () => import('pages/Index.vue')
      },
      {
        path: '/mecze',
        name: 'matches',
        component: () => import('pages/matches/MatchList.vue')
      },
      {
        path: '/boiska',
        name: 'fields',
        component: () => import('pages/fields/FieldList.vue')
      },
      {
        path: '/profile',
        name: 'users',
        component: () => import('pages/users/UserList.vue')
      },
      {
        path: '/profile/details/:id',
        name: 'user-details',
        component: () => import('pages/users/UserDetails.vue')
      }
    ]
  },
  {
    path: '/auth',
    name: 'auth',
    component: () => import('pages/auth/Auth.vue')
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
