import StorageService from './StorageService'

export default class HeaderService {
    static getJWt() {
        let token;
        if (StorageService.getUser()) {
            token = StorageService.getUser().token;
        }
        else {
            token = '';
        }
        return {
            headers: {
              Authorization: 'Bearer ' + token
            }
        };
    }
}
