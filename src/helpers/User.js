export default class User {
    constructor (userInfo) {
        this.id = userInfo.id;
        this.email = userInfo.email;
        this.name = userInfo.name;
        this.roleName = userInfo.roleName;
        this.token = userInfo.token;
    }
}
