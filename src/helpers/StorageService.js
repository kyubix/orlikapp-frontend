import User from './User';
import RoleNames from "./RoleNames";

const userKey = 'userInfo';

export default class StorageService {
    static setUserInfo(userInfo) {
        localStorage.setItem(userKey, JSON.stringify(userInfo));
    }

    static removeUserInfo() {
        localStorage.removeItem(userKey);
    }

    static getUserInfo() {
        if (localStorage.getItem(userKey)) {
            return JSON.parse(localStorage.getItem(userKey));
        }
    }

    static getUser() {
        if (this.getUserInfo()) {
            return new User(this.getUserInfo());
        }
    }

    static getLoginState() {
        if (this.getUserInfo()) {
            if (this.getUserInfo().token) {
                return true;
            }
        }
        return false;
    }

    static getToken() {
        if (this.getUserInfo()) {
            return this.getUserInfo().token;
        }
        return '';
    }

    static isAdmin() {
        if (this.getUser().roleName === RoleNames.getAdmin()) {
            return true;
        }
        return false;
    }

    static isKeeper() {
        if (this.getUser().roleName === RoleNames.getKeeper()) {
            return true;
        }
        return false;
    }

    static isUser() {
        if (this.getUser().roleName === RoleNames.getUser()) {
            return true;
        }
        return false;
    }
}
