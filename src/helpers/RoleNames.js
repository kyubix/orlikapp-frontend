const ADMIN = 'Admin';
const KEEPER = 'Kierownik boiska';
const USER = 'Zawodnik';

export default class RoleNames {
    static getAdmin() {
        return ADMIN;
    }
    static getKeeper() {
        return KEEPER;
    }
    static getUser() {
        return USER;
    }
}
