import { Notify } from 'quasar';

Notify.setDefaults({
  position: 'top',
  timeout: 3000,
  textColor: 'white',
  color: 'red',
  icon: 'error',
  actions: [{ icon: 'close', color: 'white' }]
});
