import axios from 'axios';
import StorageService from '../helpers/StorageService'

export default async ({ Vue }) => {
  Vue.prototype.$axios = axios.create({
    baseURL: 'http://localhost:5000'
    //baseURL: 'http://192.168.43.75:5000'
  })
}
